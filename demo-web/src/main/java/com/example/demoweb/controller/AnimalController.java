
package com.example.demoweb.controller;

import com.example.demoweb.model.Animal;
import com.example.demoweb.repo.AnimalRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class AnimalController {
    
    @Autowired
    AnimalRepository repository;


    @GetMapping("/animal")
    public List<Animal> getAllAnimal() {
      System.out.println("Get all Animal...");
   
      List<Animal> animal = new ArrayList<>();
      repository.findAll().forEach(animal::add);
   
      return animal;
    }

    @PostMapping(value = "/animal/create")
    public Animal postAnimal(@RequestBody Animal animal) {
    Animal _animal = repository.save(new Animal(animal.getAnimalprice(), animal.getAnimalpersonality(), animal.getAnimalname() ));
    return _animal;
  }
    @GetMapping("/animal/id/{id}")
    public Optional<Animal> findById(@PathVariable long id){
        Optional<Animal> animals = repository.findById(id);
        return animals;
  }
    

    @DeleteMapping("/animal/{id}")
    public ResponseEntity<String> deleteAnimal(@PathVariable("id") long id) {
        System.out.println("Delete Animal with ID = " + id + "...");
        repository.deleteById(id);
        return new ResponseEntity<>("Animal has been deleted!", HttpStatus.OK);
    }

    @PutMapping("/animal/{id}")
    public ResponseEntity<Animal> updateAnimal(@PathVariable("id") long id, @RequestBody Animal animal) {
        System.out.println("Update Animal with ID = " + id + "...");
    
        Optional<Animal> animalData = repository.findById(id);
    
        if (animalData.isPresent()) {
            Animal _animal = animalData.get();
                   _animal.setAnimalprice(animal.getAnimalprice());
                   _animal.setAnimalpersonality(animal.getAnimalpersonality());
                   _animal.setAnimalname(animal.getAnimalname());

        return new ResponseEntity<>(repository.save(_animal), HttpStatus.OK);
        } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
   
}