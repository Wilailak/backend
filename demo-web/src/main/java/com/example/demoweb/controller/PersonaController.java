package com.example.demoweb.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.demoweb.model.Persona;
import com.example.demoweb.repo.PersonaRepository;
import com.example.demoweb.requestmodel.LoginModelRequest;
import com.example.demoweb.responsemodel.ResponseModelLogin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class PersonaController {
    @Autowired
    PersonaRepository repository;


    @GetMapping("/customers")
    public List<Persona> getAllCustomers() {
      System.out.println("Get all Customers...");
   
      List<Persona> customers = new ArrayList<>();
      repository.findAll().forEach(customers::add);
   
      return customers;
    }
    
    @PostMapping("/customers/login")
    public ResponseModelLogin loginCustomer(@RequestBody LoginModelRequest request){
        System.err.println("Login process");
        ResponseModelLogin response = new ResponseModelLogin();
        String status = "";
        Persona person = repository.findByUsername(request.getUsername());
        if(person == null){
            response.setMessage("User Not Found");
            response.setStatus("fail");
            System.out.println("Login Message: "+response.getMessage());
           return response;
        }else{
            if(person.getPassword().equals(request.getPassword())){
              response.setMessage("Login Success");
              response.setStatus("pass");
               System.out.println("Login Message: "+response.getMessage());
              return response;
            }
            else{
              response.setMessage("password Not equals");
              response.setStatus("fail");
               System.out.println("Login Message: "+response.getMessage());
              return response;
            }
        }
    }
    

    
    @PostMapping(value = "/customers/create")
    public Persona postCustomer(@RequestBody Persona customer) {

      Persona _customer = repository.save(new Persona(
              customer.getUsername(), 
              customer.getPassword(), 
              customer.getNickname(),
              customer.getEmail()));
      return _customer;
    }
    
    @PutMapping("/customers/{id}")
    public ResponseEntity<Persona> updateCustomer(@PathVariable("id") long id, @RequestBody Persona persona) {
        System.out.println("Update Customer with ID = " + id + "...");
    
        Optional<Persona> customerData = repository.findById(id);
    
        if (customerData.isPresent()) {
            Persona _customer = customerData.get();
        _customer.setUsername(persona.getUsername());
        _customer.setPassword(persona.getUsername());
        _customer.setNickname(persona.getNickname());
        _customer.setEmail(persona.getEmail());

        return new ResponseEntity<>(repository.save(_customer), HttpStatus.OK);
        } else {
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
  

}