
package com.example.demoweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "animal")
public class Animal {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private long id;

    @Column(name = "animalprice")
    private String animalprice;

    @Column(name = "animalpersonality")
    private String animalpersonality;

    @Column(name = "animalname")
    private String animalname;

    public Animal() {

    }
    public Animal(String animalprice, String animalpersonality, String animalname) {
        this.animalprice = animalprice;
        this.animalpersonality = animalpersonality;
        this.animalname = animalname;

    }

    public long getId(){
        return id;
    }

    public void setAnimalprice(String animalprice) {
        this.animalprice = animalprice;
      }
     
      public String getAnimalprice() {
        return this.animalprice;
      }

      public void setAnimalpersonality(String animalpersonality) {
        this.animalpersonality = animalpersonality;
      }
     
      public String getAnimalpersonality() {
        return this.animalpersonality;
      }
      
        public void setAnimalname(String animalname) {
        this.animalname = animalname;
      }
     
      public String getAnimalname() {
        return this.animalname;
      }

  @Override
  public String toString() {
    return "Animal [id=" + id + ", animalprice=" + animalprice + ", animalPersonality=" + animalpersonality + ", animalname=" + animalname +"]";
  }

}
