package com.example.demoweb.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.cglib.core.EmitUtils;

@Entity
@Table(name = "personas")
@NamedQuery(name="Persona.findAll", query="SELECT p FROM Persona p")
public class Persona {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)

    private long id;

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @Column(name = "nickname")
    private String nickname;
    
    @Column(name = "email")
    private String email;

    public Persona() {

    }
    public Persona(String username, String password, String nickname, String email) {
        this.username = username;
        this.password = password;
        this.nickname = nickname;
        this.email = email;
    }

    public long getId(){
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
      }
     
      public String getUsername() {
        return this.username;
      }

      public void setPassword(String password) {
        this.password = password;
      }
     
      public String getPassword() {
        return this.password;
      }

      public void setNickname(String nickname) {
        this.nickname = nickname;
      }
     
      public String getNickname() {
        return this.nickname;
      }

      public void setEmail(String email) {
        this.email = email;
      }
     
      public String getEmail() {
        return this.email;
      }

      
  @Override
  public String toString() {
    return "Persona [id=" + id + ", username=" + username + ", password=" + password + ", nickname=" + nickname + ", email=" + email +"]";
  }

}