
package com.example.demoweb.repo;

import com.example.demoweb.model.Animal;
import java.util.List;
import org.springframework.data.repository.CrudRepository;

public interface AnimalRepository extends CrudRepository<Animal, Long>{
           List<Animal> findById(int id);
}
