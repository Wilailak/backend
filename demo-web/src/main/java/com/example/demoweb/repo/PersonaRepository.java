package com.example.demoweb.repo;
import com.example.demoweb.model.Animal;
import org.springframework.data.repository.CrudRepository;

// import java.util.List;
import com.example.demoweb.model.Persona;
import java.util.List;

public interface PersonaRepository extends CrudRepository<Persona, Long>{

    public Persona findByUsername(String username);


}